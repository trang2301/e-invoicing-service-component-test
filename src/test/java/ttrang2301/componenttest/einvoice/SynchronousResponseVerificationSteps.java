package ttrang2301.componenttest.einvoice;

import io.cucumber.core.internal.com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.ScenarioScope;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;


@ScenarioScope
public class SynchronousResponseVerificationSteps {

    private static final Charset REQUEST_BODY_CHARSET = StandardCharsets.UTF_8;
    private static final String VALID_FILE_NAME_SAMPLE = "B2B-Marketing-20211229-225959.csv";
    private static final String VALID_CONTENT_TYPE_HEADER_VALUE = "text/csv";
    public static final String A_BOUNDARY_STRING = "myBoundary";
    public static final String FEATURE_URI = "http://localhost:8080/invoice/upload";
    private final ObjectMapper objectMapper;
    private final HttpClient httpClient;
    private final MultipartHttpRequestBuilder httpRequestBuilder;
    private HttpResponse<byte[]> httpResponse;


    public SynchronousResponseVerificationSteps() {
        httpClient = HttpClient.newHttpClient();
        objectMapper = new ObjectMapper();
        httpRequestBuilder = new MultipartHttpRequestBuilder();
        httpResponse = null;
    }

    @Given("value of HTTP request header Content-Type is {string}")
    public void value_of_http_request_header_content_type_is(String contentType) {
        this.httpRequestBuilder.setContentTypeHeaderValue(contentType);
    }

    @Given("value of filename property of HTTP request header Content-Disposition is {string}")
    public void value_of_filename_property_of_http_request_header_content_disposition_is(String filename) {
        this.httpRequestBuilder.setFilename(filename);
    }

    @Given("first line of csv file is {string}")
    public void first_line_of_csv_file_is(String firstCsvLine) {
        this.httpRequestBuilder.setCsvFileContent(firstCsvLine);
    }

    @When("client sends multipart\\/form-data HTTP request with a single part named 'file'")
    public void client_sends_multipart_request() throws Throwable {
        final HttpRequest httpRequest = this.httpRequestBuilder.build();
        httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofByteArray());
    }

    @Then("client receives status code of {int}")
    public void the_client_receives_status_code_of(int statusCode) {
        assertThat(httpResponse.statusCode()).isEqualTo(statusCode);
    }

    @And("client receives body with message {string}")
    public void client_receives_body_with_message(String message) throws IOException {
        final var responseBodyAsString = new String(httpResponse.body());
        assertThat(responseBodyAsString).isNotBlank();
        final Map responseBodyAsMap = objectMapper.readValue(httpResponse.body(), Map.class);
        assertThat(responseBodyAsMap).containsKey("message");
        assertThat(responseBodyAsMap.get("message")).isNotNull();
        assertThat(responseBodyAsMap.get("message")).isEqualTo(message);
    }

    private static final class MultipartHttpRequestBuilder {

        private final URI uri;
        private final Map<String, String> headers;
        private String contentTypeHeaderValue;
        private String filename;
        private byte[] csvFileContent;

        public MultipartHttpRequestBuilder(URI uri, Map<String, String> headers) {
            this.uri = uri;
            this.headers = headers;
            this.contentTypeHeaderValue = null;
            this.filename = null;
            this.csvFileContent = null;
        }

        public MultipartHttpRequestBuilder(String uriAsString) {
            this(URI.create(uriAsString), new HashMap<>());
        }

        public MultipartHttpRequestBuilder() {
            this(FEATURE_URI);
        }

        public void setContentTypeHeaderValue(String contentTypeHeaderValue) {
            this.contentTypeHeaderValue = contentTypeHeaderValue;
        }

        public void setFilename(String filename) {
            this.filename = filename;
        }

        public void setCsvFileContent(byte[] csvFileContentAsBytes) {
            this.csvFileContent = csvFileContentAsBytes;
        }

        public void setCsvFileContent(String csvFileContentAsString) {
            setCsvFileContent(csvFileContentAsString.getBytes(REQUEST_BODY_CHARSET));
        }

        public HttpRequest build() {
            final var builder = HttpRequest.newBuilder().uri(uri);
            builder.header("Content-Type", "multipart/form-data; boundary=" + A_BOUNDARY_STRING);
            if (!CollectionUtils.isEmpty(headers)) {
                headers.forEach(builder::header);
            }
            if (contentTypeHeaderValue == null) {
                contentTypeHeaderValue = VALID_CONTENT_TYPE_HEADER_VALUE;
            }
            if (filename == null) {
                filename = VALID_FILE_NAME_SAMPLE;
            }
            final List<byte[]> bodyAsByteArrays = buildBodyAsBytes(contentTypeHeaderValue, filename, csvFileContent);
            return builder.POST(HttpRequest.BodyPublishers.ofByteArrays(bodyAsByteArrays)).build();
        }

        private static List<byte[]> buildBodyAsBytes(String contentType, String filename, byte[] csvFileContent) {
            final List<byte[]> byteArrays = new ArrayList<>();
            // Separator with boundary
            final byte[] separator = ("--" + A_BOUNDARY_STRING + "\r\nContent-Disposition: form-data; name=").getBytes(REQUEST_BODY_CHARSET);
            byteArrays.add(separator);
            byteArrays.add(("\"file\"; filename=\"" + filename
                    + "\"\r\nContent-Type: " + contentType + "\r\n\r\n").getBytes(REQUEST_BODY_CHARSET));
            if (csvFileContent != null && csvFileContent.length > 0) {
                byteArrays.add(csvFileContent);
            }
            byteArrays.add("\r\n".getBytes(REQUEST_BODY_CHARSET));
            // Closing boundary
            byteArrays.add(("--" + A_BOUNDARY_STRING + "--").getBytes(REQUEST_BODY_CHARSET));
            return byteArrays;
        }

    }

}
