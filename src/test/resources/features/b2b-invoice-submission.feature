Feature: client sends invoice submission
  Scenario: client sends multipart/form-data request with Content-Type is not text/csv
    Given value of HTTP request header Content-Type is "text"
    When client sends multipart/form-data HTTP request with a single part named 'file'
    Then client receives status code of 400
    And client receives body with message "Expected type: 'text/csv'. Actual type: 'text'"
  Scenario: client sends multipart/form-data request CSV file which contains invalid column name(s)
    Given first line of csv file is "invalidHeader1,invalidHeader2"
    When client sends multipart/form-data HTTP request with a single part named 'file'
    Then client receives status code of 400
    And client receives body with message "Expected 30 columns, but found 2 column(s)"